import React from "react";
import "./Header.css";

export default function Header() {
  return (
    <div className=" header" data-test="header">
      <i className="fab fa-youtube" data-test="youtube-icon"></i>
      <h1 className="youtube-title" data-test="youtube-title">
        YouTube
      </h1>
    </div>
  );
}
