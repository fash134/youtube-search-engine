import React from "react";
import { shallow } from "enzyme";
import Video from "./Video";
import dummyResponse from "../../redux/test/dummy-response";
import renderer from "react-test-renderer";
import { findTestAttr } from "../../../utils";

const setUp = (props = {}) => {
  const component = shallow(<Video {...props} />);
  return component;
};

describe("Video component", () => {
  let component;
  beforeEach(() => {
    component = setUp({
      videoDetails: [...dummyResponse.items],
    });
  });
  
  it("snapshot video component", () => {
    const tree = renderer.create(component).toJSON();
    expect(tree).toMatchSnapshot();
  });

  it("should render video container without error", () => {
    const wrapper = findTestAttr(component, "video-container");
    expect(wrapper.length).toBe(2);
  });
  it("should render video thumbnail without error", () => {
    const wrapper = findTestAttr(component, "image");
    expect(wrapper.length).toBe(2);
  });

  it("should render video details without error", () => {
    const wrapper = findTestAttr(component, "video-details");
    expect(wrapper.length).toBe(2);
  });
});
