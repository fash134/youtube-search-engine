import { searchActionTypes } from "../search/search-types";
import searchReducer from "../search/search-reducer";
import response from "./dummy-response";

const INITIAL_STATE = {
  login: false,
  newSearchItem: "",
  searchResult: [],
  nextPageToken: "",
  searchCount: 15,
  offset: 50,
};
describe("searchReducer", () => {
  it("should return the initial state", () => {
    expect(searchReducer(undefined, {})).toEqual(INITIAL_STATE);
  });
  it("should set login condition", () => {
    expect(
      searchReducer(INITIAL_STATE, {
        type: searchActionTypes.LOGIN,
      })
    ).toEqual({ ...INITIAL_STATE, login: !INITIAL_STATE.login });
  });

  it("should set new search title", () => {
    expect(
      searchReducer(INITIAL_STATE, {
        type: searchActionTypes.NEW_SEARCH_TITLE,
        payload: "fahad",
      })
    ).toEqual({
      ...INITIAL_STATE,
      newSearchItem: "fahad",
    });
  });

  it("should set new search result", () => {
    expect(
      searchReducer(INITIAL_STATE, {
        type: searchActionTypes.SEARCH_ITEM_RESULT,
        payload: response,
      })
    ).toEqual({
      ...INITIAL_STATE,
      searchResult: response.items,
      nextPageToken: response.nextPageToken,
    });
  });

  it("should set trending list", () => {
    expect(
      searchReducer(INITIAL_STATE, {
        type: searchActionTypes.TRENDING,
        payload: response,
      })
    ).toEqual({
      ...INITIAL_STATE,
      searchResult: response.items,
    });
  });
  it("should update search result", () => {
    expect(
      searchReducer(INITIAL_STATE, {
        type: searchActionTypes.INFINITE_SCROLL,
        payload: response,
      })
    ).toEqual({
      ...INITIAL_STATE,
      searchResult: [...INITIAL_STATE.searchResult, ...response.items],
      nextPageToken: response.nextPageToken,
    });
  });
});
