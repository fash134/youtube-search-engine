import { applyMiddleware, createStore } from "redux";
import rootReducer from "./../src/redux/root-reducer";
import thunk from "redux-thunk";

const middlewares = [thunk];
export const testStore = (initialState) => {
  const createStoreWithMiddleware = applyMiddleware(...middlewares)(
    createStore
  );
  return createStoreWithMiddleware(rootReducer, initialState);
};

export const findTestAttr = (component, attr) => {
  const wrapper = component.find(`[data-test='${attr}']`);
  return wrapper;
};
